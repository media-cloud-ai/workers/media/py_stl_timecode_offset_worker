import mcai_worker_sdk as mcai


# *******************************************************************************************************
# constants
# *******************************************************************************************************
ERROR_NOERROR = 0
ERROR_OPENINPUTFILE = -1
ERROR_OPENOUTPUTFILE = -2
ERROR_INVALID_ARGS = -3
ERROR_INVALID_FILESIZE = -9
ERROR_INVALID_GSI = -10
ERROR_INVALID_TTI = -11
ERROR_INVALID_TTICOUNT = -12
ERROR_TIMECODECONTINUITY = -20
ERROR_TIMECODEOUTOFRANGE = -21


# *******************************************************************************************************
# debug only
# *******************************************************************************************************
def outouputDebug(msg):
    print(msg)


class StlOffsetParameters(mcai.WorkerParameters):
    source_path: str
    destination_path: str
    timecode: str


# **********************************************************************************************


class StlOffsetWorker(mcai.Worker):
    def __init__(self, params, desc):
        super().__init__(params, desc)

    def process(
        self,
        handle_callback: mcai.McaiChannel,
        parameters: StlOffsetParameters,
        job_id: int,
    ):
        outouputDebug("Current Job ID: " + str(job_id))
        # print("Current Parameters: ", parameters)

        inputfile = parameters.source_path
        outputfile = parameters.destination_path
        timecode = parameters.timecode

        outouputDebug("Input File  : " + inputfile)
        outouputDebug("Output File : " + outputfile)
        outouputDebug("New Time Code : " + timecode)

        ret = self.processFile(inputfile, outputfile, timecode, handle_callback)

    # **********************************************************************************************
    # read stl n19 header
    # **********************************************************************************************
    def readGSI(self, fileHandle):
        fileHandle.seek(0, 0)
        gsi = fileHandle.read(1024)
        outouputDebug("read GSI.  Length : " + str(len(gsi)))

        return gsi

    # **********************************************************************************************
    # read stl n19 tti
    # **********************************************************************************************
    def readTTI(self, fileHandle, ttiNumber):
        fileHandle.seek(1024 + ttiNumber * 128, 0)
        tti = fileHandle.read(128)
        return tti

    # **********************************************************************************************
    # write stl n19 tti
    # **********************************************************************************************
    def writeTTI(self, fileHandle, ttiNumber, tti):
        fileHandle.seek(1024 + ttiNumber * 128, 0)
        tti = fileHandle.write(tti)
        return

    # **********************************************************************************************
    # write stl n19 tti
    # **********************************************************************************************
    def writeTTI(self, fileHandle, ttiNumber, tti):
        fileHandle.seek(1024 + ttiNumber * 128, 0)
        tti = fileHandle.write(tti)
        return

    # **********************************************************************************************
    # Time Code String to Time
    # **********************************************************************************************
    def strTCToTime(self, sTimeCode, num, den):
        hour = float(sTimeCode[0:2])
        minute = float(sTimeCode[3:5])
        second = float(sTimeCode[6:8])
        frame = float(sTimeCode[9:11])
        frame = (1 / den) * frame
        totalTime = second + (minute * 60) + (hour * 3600)
        totalTime = totalTime + frame
        return totalTime

    # **********************************************************************************************
    # Time Code Time to String
    # **********************************************************************************************
    def timeTCToStr(self, ftimeCode, num, den):
        hour = int(ftimeCode / 3600)
        ftimeCode = ftimeCode - (hour * 3600)
        minute = int(ftimeCode / 60)
        ftimeCode = ftimeCode - (minute * 60)
        second = int(ftimeCode)
        frame = ftimeCode - second
        frame = (frame + 0.01) * den

        sTimeCode = "%02d:%02d:%02d:%02d" % (hour, minute, second, frame)
        return sTimeCode

    # **********************************************************************************************
    # processFile
    # **********************************************************************************************
    def processFile(self, inputFile, outputFile, timeCode, handle_callback):
        try:
            fhi = open(inputFile, mode="rb")
        except:
            return ERROR_OPENINPUTFILE

        try:
            fho = open(outputFile, mode="wb")
        except:
            fhi.close()
            return ERROR_OPENOUTPUTFILE

        # **********************************************************************************************
        # read GSI
        # **********************************************************************************************
        gsi = self.readGSI(fhi)
        if len(gsi) != 1024:
            fhi.close()
            fho.close()
            return ERROR_INVALID_FILESIZE

        nbtti = int(gsi[238:243].decode("ascii"))
        nbst = int(gsi[243:248].decode("ascii"))

        if (nbst > nbtti) or (nbst == 0):
            fhi.close()
            fho.close()
            return ERROR_INVALID_GSI

        outouputDebug("TTI count : " + str(nbtti))
        outouputDebug("Subtitles count : " + str(nbst))

        fho.write(gsi)

        # **********************************************************************************************
        # frame rate... TO DO
        # **********************************************************************************************
        den = float(25)
        num = float(1)

        # **********************************************************************************************
        # loop
        # **********************************************************************************************

        currentTTI = 0
        firstSubtitle = True
        returnCode = ERROR_NOERROR
        lastProgress = -1

        lastfTimeCodeIn = -1
        lastfTimeCodeOut = -1

        maxTimeCode = self.strTCToTime("24:00:00:00", num, den)
        foffset = self.strTCToTime(timeCode, num, den)

        while (currentTTI < nbtti) and (returnCode == ERROR_NOERROR):
            tti = self.readTTI(fhi, currentTTI)
            if len(tti) != 128:
                returnCode = ERROR_INVALIDSIZE
            else:
                subtitleNumber = tti[2] * 256 + tti[1]
                if (subtitleNumber < 0) or (subtitleNumber > nbst):
                    returnCode == ERROR_INVALID_TTI
                else:
                    sTimeCodeIn = "%02d:%02d:%02d:%02d" % (
                        tti[5],
                        tti[6],
                        tti[7],
                        tti[8],
                    )
                    sTimeCodeOut = "%02d:%02d:%02d:%02d" % (
                        tti[9],
                        tti[10],
                        tti[11],
                        tti[12],
                    )

                    # outouputDebug("subtitle : " + str(subtitleNumber)+" "+sTimeCodeIn+" "+sTimeCodeOut  )
                    fTimeCodeIn = self.strTCToTime(sTimeCodeIn, num, den)
                    fTimeCodeOut = self.strTCToTime(sTimeCodeOut, num, den)

                    # *********************************************************************************
                    # timecode  Absolute value
                    # *********************************************************************************
                    if firstSubtitle:
                        firstSubtitle = False
                        foffset = foffset - fTimeCodeIn
                        lastfTimeCodeIn = fTimeCodeIn
                        lastfTimeCodeOut = fTimeCodeOut
                    else:
                        if fTimeCodeIn < lastfTimeCodeOut:
                            returnCode = ERROR_TIMECODECONTINUITY
                            break

                    # *********************************************************************************
                    # add / substract ?
                    # *********************************************************************************
                    fTimeCodeIn = fTimeCodeIn + foffset
                    fTimeCodeOut = fTimeCodeOut + foffset

                    #  *********************************************************************************
                    # TODO overflow 24H
                    # *********************************************************************************

                    # ...

                    if (fTimeCodeIn >= maxTimeCode) or (fTimeCodeOut >= maxTimeCode):
                        returnCode = ERROR_TIMECODEOUTOFRANGE
                        break

                    sTimeCodeIn = self.timeTCToStr(fTimeCodeIn, num, den)
                    sTimeCodeOut = self.timeTCToStr(fTimeCodeOut, num, den)

                    # outouputDebug("new subtitle : " + str(subtitleNumber)+" "+sTimeCodeIn+" "+sTimeCodeOut  )

                    newtti = bytearray(tti)
                    newtti[5] = int(sTimeCodeIn[0:2].encode())
                    newtti[6] = int(sTimeCodeIn[3:5].encode())
                    newtti[7] = int(sTimeCodeIn[6:8].encode())
                    newtti[8] = int(sTimeCodeIn[9:11].encode())

                    newtti[9] = int(sTimeCodeOut[0:2].encode())
                    newtti[10] = int(sTimeCodeOut[3:5].encode())
                    newtti[11] = int(sTimeCodeOut[6:8].encode())
                    newtti[12] = int(sTimeCodeOut[9:11].encode())

                    self.writeTTI(fho, currentTTI, newtti)

                    progress = int((currentTTI / nbtti) * 100)

                    if (lastProgress != progress) and (progress % 10) == 0:
                        lastProgress = progress
                        handle_callback.publish_job_progression(progress)

            currentTTI = currentTTI + 1

        if returnCode == ERROR_NOERROR:
            handle_callback.set_job_status(mcai.JobStatus.Completed)
        else:
            handle_callback.set_job_status(mcai.JobStatus.Error)

        fhi.close()
        fho.close()
        return returnCode


# **********************************************************************************************


def main():
    description = mcai.WorkerDescription(__package__)
    worker = StlOffsetWorker(StlOffsetParameters, description)
    worker.start()


if __name__ == "__main__":
    main()
