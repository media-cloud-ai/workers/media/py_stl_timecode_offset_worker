FROM python:3.9

WORKDIR /src

ADD py_stl_timecode_offset_worker /src/py_stl_timecode_offset_worker

COPY pyproject.toml /src

RUN pip install .

ENV AMQP_QUEUE=job_stl_timecode_offset_worker

CMD python3 py_stl_timecode_offset_worker/worker.py
